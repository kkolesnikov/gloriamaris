﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jk.Gloriamaris.SomeFirstComponent.Common;

namespace Jk.Gloriamaris.SomeFirstComponent.Core
{
    public class SomeFirstComponentCore:ISomeFirstComponent
    {
        public void DoSomething(string id)
        {
            throw new NotImplementedException();
        }

        public ClassB DoAnotherSomething(ClassA a)
        {
            throw new NotImplementedException();
        }

        public void DoAnotherAnotherSomething(ClassA a, ClassB b)
        {
            throw new NotImplementedException();
        }
    }
}
