﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jk.Gloriamaris.SomeFirstComponent.Common;
using Jk.Gloriamaris.SomeSecondComponent.Common;

namespace Jk.Gloriamaris.SomeSecondComponent.Core
{
    public class SomeSecondComponentCore: ISomeSecondComponent
    {
        private ISomeFirstComponent first;

        public SomeSecondComponentCore(ISomeFirstComponent first)
        {
            this.first = first;
        }

        public void DoSomething()
        {
            first.DoSomething("someId");
            var result = first.DoAnotherSomething(new ClassA());
            //
        }
    }
}
