﻿namespace Jk.Gloriamaris.Common
{
    public class Answer
    {
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string ErrorSubCode { get; set; }
    }


    public class Answer<TResult> : Answer
    {
        public TResult Result { get; set; }
    }
}