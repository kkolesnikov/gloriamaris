﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jk.Gloriamaris.Common.Client;
using Jk.Gloriamaris.SomeSecondComponent.Common;

namespace Jk.Gloriamaris.SomeSecondComponent.Client
{
    public class SomeSecondComponentClient:BaseClient, ISomeSecondComponent
    {
        public void DoSomething()
        {
            PostMessage(nameof(DoSomething));
        }
    }
}
