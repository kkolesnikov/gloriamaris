﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Jk.Gloriamaris.Common;
using Jk.Gloriamaris.Common.Client;
using Jk.Gloriamaris.SomeFirstComponent.Common;

namespace Jk.Gloriamaris.SomeFirstComponent.Client
{
    public class SomeFirstComponentClient:BaseClient, ISomeFirstComponent
    {
        public void DoSomething(string id)
        {
            var arg0 = new StringRef() {Ref = id};
            PostMessage(nameof(DoSomething), arg0);
        }

        public ClassB DoAnotherSomething(ClassA a)
        {
            return PostMessage<ClassA, ClassB>(nameof(DoAnotherSomething), a);
        }

        public void DoAnotherAnotherSomething(ClassA a, ClassB b)
        {
            PostMessage(nameof(DoAnotherSomething), new {arg0 = a, arg1 = b});
        }
    }
}
