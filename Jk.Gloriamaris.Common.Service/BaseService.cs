﻿using System;
using Jk.Gloriamaris.Common.Exceptions;

namespace Jk.Gloriamaris.Common.Service
{
    public class BaseService<TCore>
    {
        protected TCore core;

        public BaseService(TCore core)
        {
            this.core = core;
        }

        protected TReturn CallCoreMethod<TReturn>(TCore core, Func<TCore, TReturn> coreMethod)
        {
            return coreMethod(core);
        }

        protected void CallCoreMethod(TCore core, Action<TCore> action)
        {
            action(core);
        }


        

        protected Answer<TResult> GenerateAnswerFromException<TResult>(GloriamarisException exception)
        {
            return new Answer<TResult>
            {
                ErrorCode = exception.ErrorCode,
                ErrorSubCode = exception.ErrorSubCode,
                ErrorMessage = exception.ErrorMessage
            };
        }

        protected VoidAnswer GenerateAnswerFromException(GloriamarisException exception)
        {
            return new VoidAnswer()
            {
                ErrorCode = exception.ErrorCode,
                ErrorSubCode = exception.ErrorSubCode,
                ErrorMessage = exception.ErrorMessage
            };
        }
    }
}