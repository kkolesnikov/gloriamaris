﻿using Jk.Gloriamaris.Common;
using Jk.Gloriamaris.Common.Exceptions;
using Jk.Gloriamaris.Common.Service;
using Jk.Gloriamaris.SomeSecondComponent.Common;
using Jk.Gloriamaris.SomeSecondComponent.Core;

namespace Jk.Gloriamaris.SomeSecondComponent.Service
{
    public partial class SomeSecondComponentService : BaseService<SomeSecondComponentCore>, ISomeSecondComponentService
    {
        public VoidAnswer DoSomething()
        {
            try
            {
                CallCoreMethod(core, componentCore => componentCore.DoSomething());
                return new VoidAnswer {ErrorCode = AnswerConstants.Ok};
            }
            catch (GloriamarisException e)
            {
                return GenerateAnswerFromException(e);
            }
        }
    }
}