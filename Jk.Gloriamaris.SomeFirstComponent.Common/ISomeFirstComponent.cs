﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jk.Gloriamaris.SomeFirstComponent.Common
{
    public interface ISomeFirstComponent
    {
        void DoSomething(string id);

        ClassB DoAnotherSomething(ClassA a);

        void DoAnotherAnotherSomething(ClassA a, ClassB b);
    }
}
