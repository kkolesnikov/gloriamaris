﻿using Jk.Gloriamaris.Common;

namespace Jk.Gloriamaris.SomeFirstComponent.Common
{
    public interface ISomeFirstComponentService
    {
        VoidAnswer DoSomething(StringRef id);

        Answer<ClassB> DoAnotherSomething(ClassA a);

        VoidAnswer DoAnotherAnotherSomething(ClassA a, ClassB b);
    }
}