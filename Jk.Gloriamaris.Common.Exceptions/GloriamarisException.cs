﻿using System;

namespace Jk.Gloriamaris.Common.Exceptions
{
    public class GloriamarisException : Exception
    {
        public string ErrorCode { get; set; }
        public string ErrorSubCode { get; set; }

        public string ErrorMessage { get; set; }
    }
}