﻿using Jk.Gloriamaris.SomeFirstComponent.Core;

namespace Jk.Gloriamaris.SomeFirstComponent.Service
{
    public partial class SomeFirstComponentService
    {
        public SomeFirstComponentService(SomeFirstComponentCore core) : base(core)
        {
            this.core = core;
        }
    }
}