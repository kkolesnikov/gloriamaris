﻿using Jk.Gloriamaris.Common;
using Jk.Gloriamaris.Common.Exceptions;
using Jk.Gloriamaris.Common.Service;
using Jk.Gloriamaris.SomeFirstComponent.Common;
using Jk.Gloriamaris.SomeFirstComponent.Core;

namespace Jk.Gloriamaris.SomeFirstComponent.Service
{
    public partial class SomeFirstComponentService : BaseService<SomeFirstComponentCore>, ISomeFirstComponentService
    {
        public VoidAnswer DoSomething(StringRef id)
        {
            try
            {
                var arg0 = id.Ref;
                CallCoreMethod(core, componentCore => componentCore.DoSomething(arg0));
                return new VoidAnswer {ErrorCode = AnswerConstants.Ok};
            }
            catch (GloriamarisException e)
            {
                return GenerateAnswerFromException(e);
            }
        }

        public Answer<ClassB> DoAnotherSomething(ClassA a)
        {
            try
            {
                var result = CallCoreMethod(core, componentCore => componentCore.DoAnotherSomething(a));
                var answer = new Answer<ClassB> {Result = result, ErrorCode = AnswerConstants.Ok};
                return answer;
            }
            catch (GloriamarisException e)
            {
                return GenerateAnswerFromException<ClassB>(e);
            }
        }

        public VoidAnswer DoAnotherAnotherSomething(ClassA a, ClassB b)
        {
            try
            {
                CallCoreMethod(core, componentCore => componentCore.DoAnotherAnotherSomething(a, b));
                return new VoidAnswer { ErrorCode = AnswerConstants.Ok };
            }
            catch (GloriamarisException e)
            {
                return GenerateAnswerFromException(e);
            }
        }
    }
}