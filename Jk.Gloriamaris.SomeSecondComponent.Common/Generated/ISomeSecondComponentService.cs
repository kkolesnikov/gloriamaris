﻿using Jk.Gloriamaris.Common;

namespace Jk.Gloriamaris.SomeSecondComponent.Common
{
    public interface ISomeSecondComponentService
    {
        VoidAnswer DoSomething();
    }
}